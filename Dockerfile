#!/bin/bash
FROM openjdk:11
#ARG JAR_FILE=target/*.jar
#COPY ${JAR_FILE} app.jar
#ENTRYPOINT ["java","-jar","/app.jar"]
ADD build/libs/spring-petclinic-2.6.0.jar spring-petclinic-2.6.0.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "spring-petclinic-2.6.0.jar"]
